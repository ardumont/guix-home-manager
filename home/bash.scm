;;; Guix Home Manager.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (home bash)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu packages lxde)
  #:use-module (ice-9 match)
  #:use-module (home)
  #:export (bash-configuration
            bash-configuration-rc
            bash-configuration-profile
            bash-configuration-history

            default-bashrc
            default-bash-profile

            bash-home-type))

(define default-bashrc
  `("export SHELL

if [[ $- != *i* ]]; then
  # Non interative shell. For SSH session, load /etc/profile to get
  # PATH and other variables set up.
    [[ -n \"$SSH_CLIENT\" ]] && source /etc/profile
  return
fi

source /etc/bashrc

if [ -n \"$GUIX_ENVIRONMENT\" ]; then
        PS1='\\u@\\h \\w [env]\\$ '
else
        PS1='\\u@\\h \\w\\$ '
fi
alias ls='ls -p --color=auto'
alias ll='ls -l'
alias grep='grep --color=auto'
"))

(define default-bash-profile
  `("if [ -f ~/.bashrc ]; then . ~/.bashrc; fi
# Workaround guix bug (not adding load path before processing the command name)
export GUILE_LOAD_PATH=~/.config/guix/current/share/guile/site/3.0:$GUILE_LOAD_PATH
export GUILE_LOAD_COMPILED_PATH=~/.config/guix/current/lib/guile/3.0/site-ccache:\
$GUILE_LOAD_COMPILED_PATH
"))

(define-record-type* <bash-configuration>
  bash-configuration make-bash-configuration
  bash-configuration?
  (rc      bash-configuration-rc
           (default default-bashrc))
  (profile bash-configuration-profile
           (default default-bash-profile))
  (history bash-configuration-history))

(define bash-home-type
  (home-type
    (name 'bash)
    (extensions
      (list
        (home-extension
          (target root-home-type)
          (compute
            (lambda (config)
              (match config
                (($ <bash-configuration> rc profile history)
                 (let ((bashrc (apply mixed-text-file "bashrc" rc))
                       (bash-profile (apply mixed-text-file "bash-profile" profile)))
                   `((".bash_history" ,history)
                     (".bashrc" ,bashrc)
                     (".bash_profile" ,bash-profile))))))))))))
