;;; Guix Home Manager.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (home neovim)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (gnu packages lxde)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (home)
  #:export (neovim-configuration
            neovim-configuration-init
            neovim-configuration-plugins

            default-init.vim

            neovim-home-type))

(define default-init.vim
  `(""))

(define-record-type* <neovim-configuration>
  neovim-configuration make-neovim-configuration
  neovim-configuration?
  (init    neovim-configuration-init
           (default default-init.vim))
  (plugins neovim-configuration-plugins
           (default '())))

(define neovim-home-type
  (home-type
    (name 'neovim)
    (extensions
      (list
        (home-extension
          (target root-home-type)
          (compute
            (lambda (config)
              (match config
                (($ <neovim-configuration> init plugins)
                 (let ((init.vim
                         (apply mixed-text-file "init.vim"
                                (append
                                  (fold
                                    (lambda (plugin result)
                                      (append
                                        result
                                        (list
                                          "set runtimepath+="
                                          (file-append plugin "/share/nvim/site")
                                          "\n")))
                                    '()
                                    plugins)
                                  init))))
                   `((".config/nvim/init.vim" ,init.vim))))))))))))
