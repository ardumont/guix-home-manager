;;; Guix Home Manager.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (home keepassxc)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu packages lxde)
  #:use-module (ice-9 match)
  #:use-module (home utils)
  #:use-module (home)
  #:export (keepassxc-configuration
            keepassxc-last-databases
            keepassxc-last-dir
            keepassxc-last-opened-databases
            keepassxc-show-toolbar?
            keepassxc-hide-passwords?
            keepassxc-hide-usernames?
            keepassxc-eaasci?
            keepassxc-ensure-every?
            keepassxc-exclude-alike?
            keepassxc-password-length
            keepassxc-use-lower-case?
            keepassxc-use-numbers?
            keepassxc-use-special-chars?
            keepassxc-type
            keepassxc-use-upper-case?
            keepassxc-word-count
            keepassxc-wordlist
            keepassxc-word-separator

            keepassxc-home-type))

(define-record-type* <keepassxc-configuration>
  keepassxc-configuration make-keepassxc-configuration
  keepassxc-configuration?
  (last-databases        keepassxc-last-databases
                         (default '()))
  (last-dir              keepassxc-last-dir
                         (default #f))
  (last-opened-databases keepassxc-last-opened-databases
                         (default '()))
  (show-toolbar?         keepassxc-show-toolbar?
                         (default #t))
  (hide-passwords?       keepassxc-hide-passwords?
                         (default #t))
  (hide-usernames?       keepassxc-hide-usernames?
                         (default #f))
  (ensure-every?         keepassxc-ensure-every?
                         (default #t))
  (exclude-alike?        keepassxc-exclude-alike?
                         (default #t))
  (password-length       keepassxc-password-length
                         (default 28))
  (use-eascii?           keepassxc-use-eascii?
                         (default #f))
  (use-lower-case?       keepassxc-use-lower-case?
                         (default #t))
  (use-upper-case?       keepassxc-use-upper-case?
                         (default #t))
  (use-numbers?          keepassxc-use-numbers?
                         (default #t))
  (use-special-chars?    keepassxc-use-special-chars?
                         (default #f))
  (type                  keepassxc-type
                         (default 0))
  (word-count            keepassxc-word-count
                         (default 7))
  (wordlist              keepassxc-wordlist
                         (default "eff_large.wordlist"))
  (word-separator        keepassxc-word-separator
                         (default " ")))

(define keepassxc-home-type
  (home-type
    (name 'keepassxc)
    (extensions
      (list
        (home-extension
          (target root-home-type)
          (compute (lambda (config)
                     (let ((keepassxc.ini
                             (make-ini-file "keepassxc.ini"
                              `(("General"
                                 (("LastDatabases"
                                   ,(string-join
                                      (keepassxc-last-databases config)
                                      ", "))
                                  ("LastDir"
                                   ,(or (keepassxc-last-dir config) ""))
                                  ("LastOpenedDatabases"
                                   ,(string-join
                                      (keepassxc-last-opened-databases config)
                                      ", "))))
                                ("GUI"
                                 (("HidePasswords"
                                   ,(keepassxc-hide-passwords? config))
                                  ("HideUsernames"
                                   ,(keepassxc-hide-usernames? config))))
                                ("generator"
                                 (("EASCII"
                                   ,(keepassxc-use-eascii? config))
                                  ("EnsureEvery"
                                   ,(keepassxc-ensure-every? config))
                                  ("ExcludeAlike"
                                   ,(keepassxc-exclude-alike? config))
                                  ("Length"
                                   ,(keepassxc-password-length config))
                                  ("LowerCase"
                                   ,(keepassxc-use-lower-case? config))
                                  ("Numbers"
                                   ,(keepassxc-use-numbers? config))
                                  ("SpecialChars"
                                   ,(keepassxc-use-special-chars? config))
                                  ("Type"
                                   ,(keepassxc-type config))
                                  ("UpperCase"
                                   ,(keepassxc-use-upper-case? config))
                                  ("WordCount"
                                   ,(keepassxc-word-count config))
                                  ("WordList"
                                   ,(keepassxc-wordlist config))
                                  ("WordSeparator"
                                   ,(keepassxc-word-separator config))))))))
                       `((".config/keepassxc/keepassxc.ini" ,keepassxc.ini))))))))))
