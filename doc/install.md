Installation Guide
==================

Installing the Channel
----------------------

This is a Guix channel. You will first need to
[install](https://www.gnu.org/software/guix/download/) Guix itself. Then, simply
create a new `~/.config/guix/channels.scm` file with this content:

```scheme
(cons* (channel
        (name 'guix-home-manager)
        (url "https://framagit.org/tyreunom/guix-home-manager.git"))
       %default-channels)
```

Then run `guix pull` to pull the new channel.

If the file already exist, simply add the `(channel)` form above to the list
of channels in the file. Make sure you use `(cons*)` (with a star) and not
`(cons)` since the later can only take one channel and %default-channels as
argument, whereas the first can take as many channels as you'd like before
%default-channels.

### Important checks ###

Make sure your guix environment is set up properly. You need to have
`~/.config/guix/current` as the **first** item in your `$PATH` or you're going
to run into troubles. Additionnaly, after running `guix pull`, make sure you
run `hash guix` in any open terminal to make sure bash's cache is cleared of
the old guix binary location.

Usage
-----

### Making some room in your home directory

Your home directory will be completely taken over by Guix.  In particular, when
using the home manager, your home directory is entirely read-only.  A read-only
home directory is not very useful though, so users of the home manager will have
to use a separate directory for their documents, caches and states.  This is
typically `/data/alice` for user alice.

It is not required to set up that directory beforehand, but if you do, you will
not be able to use the home manager until you have completely wiped-out your
home directory (i.e. transfered it to the new directory).  If the directory
does not yet exist, your current home directory is automatically renamed to
that directory, and the home manager starts working.

Basically, you will run (as root):

```bash
mkdir /data
mv /home/alice /data/alice
```

Once that is done, some parts of your home directory will still have to be
read-write. This is mostly `~/.cache`, `~/.local` but also `~/.guix-profile` and
`~/.config/guix`. Inside your new data directory, create them like this, as your
regular user (alice in this example):

```bash
cd /data/alice
mkdir-p .local/share .cache .config
```

Since you have moved your entire home directory, make sure you can still access
your own copy of guix and your user profile by (temporarily) setting your
`$PATH` (make sure it starts with `/data/alice/.config/guix/current/bin`) and
by sourcing the profile with `export GUIX_PROFILE=/data/alice/.guix-profile;
source $GUIX_PROFILE/etc/profile`.  You might also need to run `hash -r`
(no output) for bash to clear all its memorized binary locations.

### Creating the first home generation

To create your first home configuration, you must create a configuration file.
For instance, create `/data/alice/.config/guix/home.scm`:

```scheme
(use-modules (home))

(home
  (data-directory "/data/alice"))
```

This will generate a completely empty home, except for essential configurations,
i. e. writable XDG directories as well as essential guix symlinks.

See the documentation to learn more about the configuration system.

To build your first generation of your home environment, run as your regular
user:

```bash
guix home reconfigure /data/alice/.config/guix/home.scm
```

That's it!
