Neovim
======

Neovim is a fork of vim, a text editor.  This lets you configure it and manage
plugins.

Main Configuration
------------------

Neovim is configured by using the `neovim-home-type` service type.

**Scheme Procedure**: neovim-home-type

The type of service that generates configuration files for neovim.  Its value
is a neovim-configuration object.

**Data Type**: neovim-configuration

Data type that represents the Neovim configuration.  This data type has the
following fields:

* **init** (default *default-init*): the content of the `init.vim` file.  This
  is a list of strings and file-like objects.
* **plugins** (default '()): the list of plugins you wish to use.  They will
  be added to the top of the `init.vim` file.

Example Configuration
---------------------

```scheme
(user-home
  neovim-home-type
  (neovim-configuration
    (init '("set colorcolumn=80\n"))
    (plugins (list neovim-syntastic))))
```
