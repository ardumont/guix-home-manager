Documentation
=============

Ceci est la page principale de la documentation du gestionnaire du dossier
personnel. Guix Home Manager est une extension non officielle du gestionnaire
de paquets Guix. Vous devriez l'installer et vous familiariser avec lui avant
d'essayer cette extension.

Le gestionnaire de dossier personnel est encore très jeune et en cours de
développement, donc attendez-vous à ce que certaines parties manquent encore.

Fonctionnement de cette documentation
-------------------------------------

Cette documentation est divisée en plusieurs fichiers Markdown. Ce fichier est
le fichier principal. La plupart des fichiers parlent de la configuration de
programmes particuliers. Les autres parlent surtout de problèmes génériques
et de cas d'utilisation. Ils sont tous référencés à partir d'ici.

### Traductions

Cette documentation est traduite dans d'autres langues. Pour voir la documentation
dans votre langue à partir de la version anglaise, ajoutez simplement le suffixe
à la fin du nom de fichier, juste avant l'extension `.md`.

* For the English version, see [README.md](doc/README.md)
