Documentation
=============

This is the main page for the documentation of the Guix Home Manager.
Guix Home Manager is an unofficial extension to the Guix package
manager. You should install it and get more familiar with it before
you try this.

The Home Manager is still quite young and under development, so
expect a lot of missing features.

Layout of this Documentation
----------------------------

This documentation is divided into many Markdown files. This file is the
main file. Most files will talk about specific configurations for specific
software. Others will talk about generic issues and use cases. They are all
referenced here.

### Translations

This documentation is translated in other languages. To view the documentation
in your language, simply add the language suffix at the end of the file name,
just before the `.md` extension.

* Pour le français, voir [README.fr.md](doc/README.fr.md)

### Specific Software Configuration

See the [installation guide](doc/install.md) to learn more about how to manage
your configuration and install your first managed home. You will learn that you
need a configuration file, usually `/data/alice/.config/guix/home.scm`. The rest
of this file is a set of links to specific and generic configuration functions
that you can add to that file. Whenever you see a procedure whose name ends
with "-home", you know that you can use it in the list of configurations, like
this:

```scheme
(home
  (data-directory "/data/alice")
  (configurations
    (list
      (user-home something-home-type ...)
      (user-home something-else-home-type ...)
      (user-home other-stuff-home-type ...)
      (user-home yet-another-config-home-type ...)
      ...)))
```

#### Desktop and Window Managers

* [Openbox](doc/openbox.md)

#### Communication Software

* [Hexchat](doc/hexchat.md)

#### Security Software

* [KeepassXC](doc/keepassxc.md)
* [OpenSSH](doc/ssh.md)

#### Shell Software

* [Bash](doc/bash.md)
* [Neovim](doc/neovim.md)

### Common Issues

#### No Sound on the System

This is an issue with pulseaudio. Pulesaudio will never want to start when your
home directory is read-only, even though all its configuration files and
the files it will want to create are available read-write. To work around this,
it is advised to run pulseaudio from the command line in this way:

```bash
HOME=/tmp/pa `guix build pulseaudio`/bin/pulseaudio --start
```

Unfortunately, that means you cannot configure pulseaudio through Guix. You can
also add a similar line to your desktop or window manager's configuration to
instruct it to start pulseaudio. However, pulseaudio sometimes crashes for non
obvious reasons, and no graphical program will be able to restart it automatically
with a proper configuration.
