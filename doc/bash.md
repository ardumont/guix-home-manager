Bash
====

Bash is the default shell on Guix.  This lets you configure your shell
environment and set it up properly.

Main Configuration
------------------

Bash is configured by using the `bash-home-type` service type.

**Scheme Procedure**: bash-home-type

The type of service that generates configuration files for bash.  Its value
is a bash-configuration object.

**Data Type**: bash-configuration

Data type that represents the Bash configuration.  This data type has the
following fields:

* **rc** (default *default-bashrc*): the content of the `.bashrc` file.  This is
  a list of strings and 
* **profile** (default *default-bash-profile*): the content of the `.bash_profile`
  file.
* **history** (default #f): the location of the `.bash_history` file for saving
  history.  The location must be read-write accessible to your user.

Example Configuration
---------------------

```scheme
(user-home
  bash-home-type
  (bash-configuration
    (rc (append default-bashrc
                '("alias vim=nvim")))
    (profile (append default-bash-profile
                     `("EDITOR=nvim\n"
		       ;; Minetest would not work without this :/
                       "export MINETEST_SUBGAME_PATH="
		       ,(@@ (gnu packages games) minetest-data)
		       "/share/minetest/games/")))
    (history "/data/alice/.local/share/bash/history")))
```
