Keepassxc
=========

Keepassxc is a password manager.  Parts of the configuration file is used for
storing states.  Unfortunately, we cannot store parts of that file as read-write
and the rest as read-only.  We treat this file as a static configuration file,
but you can also create a symlink with the home utilities if you prefer.

Main Configuration
------------------

The main configuration is not complete compared to the configuration possibilities
of keepassxc.  It currently implemets most of the configuration, but excludes
a few difficult parts.

**Scheme Variable**: keepassxc-home-type

The type of the service that generates a configuration file for keepassxc.  Its
value is a keepassxc-configuration object.

**Data Type**: keepassxc-configuration

Data type that represents the Keepassxc configuration.  This data type has the
following fields:

* **last-databases** (default '()): A list of databases the user last tried to
  open.  This should probably set to the list of databases you want to use.
* **last-dir** (default #f): The last directory opened by the user.  This
  should probably set to your data directory, `/data/alice` in the installation
  example.
* **last-databases** (default '()): A list of databases the user last opened.
  This should probably set to the list of databases you want to use.
* **show-toolbar?** (default #t): Undocumented.
* **hide-passwords?** (default #t): Whether to show passwords on the GUI.
* **hide-usernames?** (default #f): Whether to show user names on the GUI.
* **ensure-every?** (default #t): Whether to use every class of characters in
  password, or allow passwords that don't contain every enables classes.
* **exclude-alike?** (default #t): Whether to prevent using similar characters.
* **password-length** (default 28): The default generated password length.
* **use-eascii?** (default #f): Whether to use extended ascii in generated
  passwords.
* **use-lower-case?** (default #t): Whether to use lower case letters in
  generated passwords.
* **use-upper-case?** (default #t): Whether to use upper case letters in
  generated passwords.
* **use-numbers?** (default #t): Whether to use numbers in generated passwords.
* **use-special-chars?** (default #f): Whether to use special characters in
  generated passwords.
* **type** (default 0): Undocumented.
* **word-count** (default 7): Undocumented.
* **wordlist** (default "eff_large.wordlist"): Undocumented.
* **word-separator** (default " "): Undocumented.

Example
-------

```scheme
(user-home
  keepassxc-home-type
  (keepassxc-configuration
    (last-databases '("/data/alice/pass.kbdx"))
    (last-dir "/data/alice")
    (last-opened-databases '("/data/alice/pass.kbdx"))))
```
